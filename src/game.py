## Import clearscreen REPL function
from helperfunctions import clearscreen

## Import Player and Dice class
from player import Player
from dice import Dice

## Import console print functions
from console_print import print_dice, print_list_of_choices, print_yahtzee_banner, print_board, print_winner

## Import functions
from dice import ones, twos, threes, fours, fives, sixes, a_pair, two_pairs, three_of_a_kind, four_of_a_kind, small_straight, large_straight, full_house, chance, yahtzee
from function_dummies import banner, player_names, sub_sum, bonus, total

## Regular expression
import re


##################################################################################
################################# Category lists #################################
##################################################################################
GAME_CATEGORIES   = [ones, twos, threes, fours, fives, sixes,
                     a_pair, two_pairs, three_of_a_kind, four_of_a_kind,
                     small_straight, large_straight, full_house, chance, yahtzee]

PLAYER_CATEGORIES = [ones, twos, threes, fours, fives, sixes,
                     sub_sum, bonus,
                     a_pair, two_pairs, three_of_a_kind, four_of_a_kind,
                     small_straight, large_straight, full_house, chance, yahtzee,
                     total]

BOARD_LAYOUT      = [banner, player_names, banner,
                     ones, twos, threes, fours, fives, sixes,
                     banner, sub_sum, bonus, banner,
                     a_pair, two_pairs, three_of_a_kind, four_of_a_kind,
                     small_straight, large_straight, full_house, chance, yahtzee,
                     banner, total, banner] 
##################################################################################
##################################################################################


class Game:
    """This class is the game class and it have an number of players and dice.
    It also knows about the board_layout and categories."""

    def __init__(self,
                 game_categories = GAME_CATEGORIES,
                 player_categories = PLAYER_CATEGORIES,
                 board_layout = BOARD_LAYOUT):
        """The game initializor. Sets up the dice and an empty players list.

        Args:
            game_categories: A list of all yahtzee categories (Default: GAME_CATEGORIES)
            player_categories: A list of all game_categories + sub_sum, bonus and total (Default: PLAYER_CATEGORIES)
            board_layout: A list of the board_layout. Typically used for printing (Default: BOARD_LAYOUT)
        """
        self._GAME_CATEGORIES = game_categories
        self._PLAYER_CATEGORIES = player_categories
        self._BOARD_LAYOUT = board_layout
        
        # Create a empty list with players
        self._players = []
        
        # Create the dice
        self._dice = Dice()


    def start_up(self):
        """Call this function to start the Yahtzee game.
        It asks for the number of players and takes there names.
        After this it starts the game"""
        print_yahtzee_banner()
        num_players = re.findall('\d+', input("Number of players: "))
        if len(num_players) == 0:
            num_players = 2
        else:
            num_players = int(num_players[0])
    
        ## Get the name of the players and create the players
        self._players = []
        print_yahtzee_banner()
        print("Please write the names of the players\n")
        for player_idx in range(1, num_players + 1):
            player_name = input("Player {0}: ".format(player_idx))
            self._players.append(Player(player_name, self._GAME_CATEGORIES, self._PLAYER_CATEGORIES))
    
        ## Start the game with the players
        self._game_play()
    

    def _game_play(self):
        """This is the game-play function"""
        # A yahtzee round takes 15 turns
        for round in range(15):
            for player in self._players:
                # Roll the dice
                self._dice_rolling(player)
    
                # Deside how it should be saved
                self._choose_how_to_save_dice(player)
    
        # Print the winner
        print_yahtzee_banner()
        self._game_print_board()
        print_winner(self._players)


    def _dice_rolling(self, player):
        """Takes care of rolling the dice three times.
        After each time it askes the player which dice to save.
        
        Args:
            player: A Player class instans
        """
        # Just make sure that the get new values on the dice
        self._dice.roll_all()
    
        print() # new line
    
        # The player can roll the dice two times
        for turn in range(2):
            clearscreen()
            self._game_print_board()
            print("{0} is rolling, turns left: {1}".format(player.get_name(), 2 - turn))
            print_dice(self._dice.get_dice_with_rotation())
            # Input with regular expression getting ints from str-input
            dice_to_save_idx = [int(val)-1 for val in re.findall('\d', input("Which dice to save (e.g. 135)? ")) if int(val) > 0 and int(val) < 6]
            if len(dice_to_save_idx) == 5:
                break # If all dice are saved then the player is happy with that he/she has got
            self._dice.roll(dice_to_save_idx)
        
    
    def _choose_how_to_save_dice(self, player):
        """This function looks at the current dice and displays a list
        of categories where this dice can be put and how much points it gives.
        The user is asked to say which option is that is best and then the function
        saves the dice to this category.

        Args:
            player: A Player class instans
        """
        clearscreen()
        self._game_print_board()
        print_dice(self._dice.get_dice_with_rotation())
        choices_list = print_list_of_choices(self._dice.get_dice(), player, self._GAME_CATEGORIES)
        
        the_choice = input("How do you want to save the dice? ")
        if the_choice == "":
            the_choice = 0
        else:
            the_choice = int(the_choice) - 1
        
        _ , chosen_category = choices_list[the_choice] # choices_list is a list of tuples
        player.use_category(chosen_category, self._dice.get_dice())
    

    def _game_print_board(self):
        """Helper function for printing a board"""
        print_board(self._players, self._BOARD_LAYOUT, self._PLAYER_CATEGORIES)
    
    
    