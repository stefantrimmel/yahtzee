"""This module have free functions for printing to the REPL (console)"""

from helperfunctions import clearscreen
from function_dummies import banner, player_names, total

from operator import itemgetter


def scores_in_categories(dice_list, categories):
    """Takes the dice list and categories and returns a sorted list with highest sum value first
    
    Args:
        dice_list: An integer list of dice
        categories: A list of the categories to iterate over
    
    Returns:
        A sorted list of tuples(sum_value, category). Highest sum in front of the list.
    """
    scores = [(category(dice_list), category) for category in categories]
    return sorted(scores, reverse=True, key=itemgetter(0))


def print_list_of_choices(dice_list, player, categories):
    """Prints which choices for this player to pick from with these dice.
    
    Args:
        dice_list: A integer list of die
        player: A Player instans
        categories: A list of categories (typically game categories)
    """
    available_categories = []
    for idx, category in enumerate(categories):
        if player.is_category_used(category) == False:
            available_categories.append(categories[idx])
    choices_list = scores_in_categories(dice_list, available_categories)
    
    longest_line = 0
    # Reserve a place for the top-banner, spacing line, choices-line and spacing line
    lines = ["+---- TOP-BANNER-PLACEHOLDER ----+", "| ... |", "| CHOOSES-LINE |", "| ... |"]

    for idx, (score, category) in enumerate(choices_list): # enumerate iterator gives index, value. value is a tuple of (score, category-function)
        s_on_noun = ""
        if score > 1:
            s_on_noun = "s"
        output = '| {0}. "{1}" gives {2} point{3} '.format(idx+1, category.__name__.replace("_", " ").capitalize(), score, s_on_noun)
        lines.append(output)
        if longest_line < len(output):
            longest_line = len(output)

    # Insert spaces + "|" in the back of each line so that they match the longest_line
    for idx, line in enumerate(lines):
        if len(line) < longest_line:
            lines[idx] += " " * (longest_line - len(line)) + "|"
        else:
            lines[idx] += "|"
    # We added the | to the longest line so we need to increase it by one
    longest_line += 1
     
    lines[0] = "+" + "-" * (longest_line - 2) + "+"
    lines[1] = "|" + " " * (longest_line - 2) + "|"
    lines[2] = "| These are your choices:"
    lines[2] += " " * (longest_line - len(lines[2]) - 1) + "|"
    lines[3] = "|" + " " * (longest_line - 2) + "|"
    # Add these lines to the back of list as well
    lines.append(lines[1])
    lines.append(lines[0])
    to_print = '\n'.join(lines)
    print(to_print)
    print()

    # We return the choices_list to the caller so that the caller know how it looks as well
    return choices_list


def print_board(players, board_layout, player_categories):
    """Prints the status of the yahtzee board to the REPL with categories and players
    
    Args:
        players: A list of players
        board_layout: A list of the board layout
        player_categories: A list of the player categories.
    """
    num_of_players = len(players)
    players_col_width = [len(player.get_name())+2 for player in players]
    max_board_category_len = max([len(board_category.__name__) for board_category in board_layout])

    # banner is something like -->  +-----+---+---+---+---+
    board_banner = "".join(["+-" + "-" * max_board_category_len + "-+"] + ["-" * width + "+" for width in players_col_width])
    # player_banner is something like -->  | Player | Name1 | Name2 | ... | NameN | 
    player_banner = "".join(["| Player" + " " * (max_board_category_len - len("Player")) + " |"] + [" " + player.get_name() + " |" for player in players])

    # Creating empty list, things will get appended in the loop
    lines = []

    for category in board_layout:
        if category in [banner]:
            lines.append(board_banner)
        elif category in [player_names]:
            lines.append(player_banner)
        elif category in player_categories:
            output = '| {0} '.format(category.__name__.replace("_", " ").capitalize())  # Category_name
            output += " " * (max_board_category_len - len(category.__name__)) + "|"     # Spaces from category to | (including |)
            
            for player in players:
                if player.is_category_used(category):
                    output += " " * ( (len(player.get_name())+2)//2 - 2 ) # Spaces before digit ( // <- integer division)
                    if player.get_saved_value(category) < 10:
                        output += "  "
                    elif player.get_saved_value(category) < 100:
                        output += " "
                    output += str(player.get_saved_value(category))
                    spaces_after_digit = (len(player.get_name())+2)//2
                    if len(player.get_name())%2 == 0:
                        spaces_after_digit -= 1
                    output += " " * spaces_after_digit + "|"  # Spaces and | after digit
                else:
                    output += " " * (len(player.get_name())+2) + "|"
            
            # Append the created line
            lines.append(output)

    to_print = "\n".join(lines)
    print(to_print)
    print()


def print_dice(dice_with_rotation):
    """Takes a list(tuple(die, rotation), ...) and prints a nice dice to the REPL
    
    Args:
        dice_with_rotation: A list with (die, rotation) tuples. Die-value and rotation are integers
    """
    std_space = "   "
    dice_to_print = []
    for row in range(5): # Console row
        for idx, (die, rotation) in enumerate(dice_with_rotation):
            if row == 0 or row == 4:
                dice_to_print.append(std_space + " ----- ")
            else:
                if die == 1:
                    if row == 1:
                        dice_to_print.append(std_space + "|     |")
                    elif row == 2:
                        dice_to_print.append(std_space + "|  o  |")
                    elif row == 3:
                        dice_to_print.append(std_space + "|     |")
                if die == 2:
                    if rotation == 1:
                        if row == 1:
                            dice_to_print.append(std_space + "|    o|")
                        elif row == 2:
                            dice_to_print.append(std_space + "|     |")
                        elif row == 3:
                            dice_to_print.append(std_space + "|o    |")
                    else:
                        if row == 1:
                            dice_to_print.append(std_space + "|o    |")
                        elif row == 2:
                            dice_to_print.append(std_space + "|     |")
                        elif row == 3:
                            dice_to_print.append(std_space + "|    o|")
                if die == 3:
                    if rotation == 1:
                        if row == 1:
                            dice_to_print.append(std_space + "|o    |")
                        elif row == 2:
                            dice_to_print.append(std_space + "|  o  |")
                        elif row == 3:
                            dice_to_print.append(std_space + "|    o|")
                    else:
                        if row == 1:
                            dice_to_print.append(std_space + "|    o|")
                        elif row == 2:
                            dice_to_print.append(std_space + "|  o  |")
                        elif row == 3:
                            dice_to_print.append(std_space + "|o    |")
                if die == 4:
                    if row == 1:
                        dice_to_print.append(std_space + "|o   o|")
                    elif row == 2:
                        dice_to_print.append(std_space + "|     |")
                    elif row == 3:
                        dice_to_print.append(std_space + "|o   o|")
                if die == 5:
                    if row == 1:
                        dice_to_print.append(std_space + "|o   o|")
                    elif row == 2:
                        dice_to_print.append(std_space + "|  o  |")
                    elif row == 3:
                        dice_to_print.append(std_space + "|o   o|")
                if die == 6:
                    if rotation == 1:
                        if row == 1:
                            dice_to_print.append(std_space + "|o   o|")
                        elif row == 2:
                            dice_to_print.append(std_space + "|o   o|")
                        elif row == 3:
                            dice_to_print.append(std_space + "|o   o|")
                    else:
                        if row == 1:
                            dice_to_print.append(std_space + "|o o o|")
                        elif row == 2:
                            dice_to_print.append(std_space + "|     |")
                        elif row == 3:
                            dice_to_print.append(std_space + "|o o o|")
            
            # Add new line after last die
            if idx == 4:
                dice_to_print.append("\n")
    
    dice_to_print.append("\n")
    dice_to_print.append(std_space + "   1  " + std_space + "    2   " + std_space + "   3   " + std_space + "   4   " + std_space + "   5\n")

    to_print = "".join(dice_to_print)
    print(to_print)
    print()


def print_yahtzee_banner():
    """Clears the REPL and prints the yahtzee banner"""
    clearscreen()
    print("####################################")
    print("#    __                      __    #")
    print("#   /\_\   Python Yahtzee   /\_\   #")
    print("#   \/_/   Stefan Trimmel   \/_/   #")
    print("#                                  #")
    print("####################################\n\n")


def print_winner(players):
    """Prints a nice first price and prints the name and score of the winner
    
    Args:
        players: A list of Player
    """
    first_price = ['.-..-""``""-..-.',
                   "|(`\`'----'`/`)|",
                   " \\\\ ;:.    ; // ",
                   "  \\\\|%.    |//  ",
                   "   )|%:    |(   ",
                   " ((,|%.    |,)) ",
                   "  '-\::.   /-'  ",
                   "     '::..'     ",
                   "       }{       ",
                   "      {__}      ",
                   "     /    \     ",
                   "    |`----'|    ",
                   "    | [#1] |    ",
                   "    `.____.'    "]

    score, name = max([(player.get_saved_value(total), player.get_name()) for player in players], key=itemgetter(0))
    winner_text = "|| {0} wins with the score {1} ||".format(name, score)
    winner_text_banner = "=" * len(winner_text)

    space_width = len(winner_text)//2 - len(first_price[0])//2
    spaces = " " * space_width 

    lines = []

    for line in first_price:
        lines.append(spaces + line)

    lines.append("")
    lines.append(winner_text_banner)
    lines.append(winner_text_banner)
    lines.append(winner_text)
    lines.append(winner_text_banner)
    lines.append(winner_text_banner)

    to_print = "\n".join(lines)
    print(to_print)
    print()