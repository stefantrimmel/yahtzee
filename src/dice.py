"""This module both have some free functions that take a dice_list as argument
and also a class that holds information about a dice

It would have been possible  to put the free functions inside the class
but having them as free functions adds value in the game board setup.
"""

import random

def dice_counts(dice_list):
    """Make a dictionary of how many of each value are in the dice
    
    Args:
        dice_list: a list of 5 integers indicating the dice rolled

    Returns:
        A dictionary of how many of each value are in the dice_list"""
    return {x: dice_list.count(x) for x in range(1, 7)}


def sum_of_equals(dice_list, value_to_sum):
    """This is a helper function for ones..sixes.
       It returns the sum of value_to_sum in the dice list.

    Args:
        dice_list: a list of 5 integers indicating the dice rolled
        value_to_sum: The die to look for among the dice and to sum
    
    Returns:
        The sum of the dice in the list with the value_to_sum number"""
    return dice_counts(dice_list)[value_to_sum] * value_to_sum


def ones(dice_list):
    """Sum of all ones

    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        The sum of the dice in the list with the value 1"""
    return sum_of_equals(dice_list, 1)


def twos(dice_list):
    """Sum of all twos

    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        The sum of the dice in the list with the value 2"""
    return sum_of_equals(dice_list, 2)


def threes(dice_list):
    """Sum of all threes

    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        The sum of the dice in the list with the value 3"""
    return sum_of_equals(dice_list, 3)


def fours(dice_list):
    """Sum of all fours

    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        The sum of the dice in the list with the value 4"""
    return sum_of_equals(dice_list, 4)


def fives(dice_list):
    """Sum of all fives

    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        The sum of the dice in the list with the value 5"""
    return sum_of_equals(dice_list, 5)


def sixes(dice_list):
    """Sum of all sixes

    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        The sum of the dice in the list with the value 6"""
    return sum_of_equals(dice_list, 6)


def a_pair(dice_list):
    """Returns the sum of the biggest pair
    
    Args:
        dice_list: a list of 5 integers indicating the dice rolled
        
    Returns:
        The integer sum of the biggest pair among the dice"""
    counts = dice_counts(dice_list)
    sum_of_biggest_pair = 0
    for key in counts.keys(): # The key is the die number and value is the number of dice of this kind
        if counts[key] >= 2 and key*2 > sum_of_biggest_pair: # Two equal and die*2 is bigger than already saved
            sum_of_biggest_pair = key*2
    return sum_of_biggest_pair


def two_pairs(dice_list):
    """Returns the sum of the two pairs if there is any

    Args:
        dice_list: a list of 5 integers indicating the dice rolled

    Returns:
        The integer sum of the two pairs"""
    counts = dice_counts(dice_list)
    pairs_found = 0
    sum_of_pairs = 0
    for key in counts.keys():
        if counts[key] >= 4:
            pairs_found = 2
            sum_of_pairs = key*4
            break  # We have 4 equal dice which can be represented as 2 pairs
        if counts[key] >= 2:
            pairs_found += 1
            sum_of_pairs += key*2
    
    if pairs_found != 2: # If we didn't find two pairs then this sum is 0
        sum_of_pairs = 0

    return sum_of_pairs


def three_of_a_kind(dice_list):
    """Returns the sum of the three of a kind
    
    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        The integer sum of the three of a kind"""
    counts = dice_counts(dice_list)
    if 3 in counts.values():
        # [...] gives a list with die_val*3 as value
        # [...][0] returns just the die_val*3 as integer
        return [die_val*3 for die_val, num_dice in counts.items() if num_dice == 3][0]
    else:
        return 0


def four_of_a_kind(dice_list):
    """Returns the sum of the four of a kind
    
    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        The integer sum of the four of a kind"""
    counts = dice_counts(dice_list)
    if 4 in counts.values():
        # [...] gives a list with die_val*4 as value
        # [...][0] returns just the die_val*4 as integer
        return [die_val*4 for die_val, num_dice in counts.items() if num_dice == 4][0]
    else:
        return 0


def full_house(dice_list):
    """Returns the sum of the full house
    
    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        The integer sum of full house"""
    counts = dice_counts(dice_list)
    if 2 in counts.values() and 3 in counts.values():
        return sum(dice_list)
    else:
        return 0


def small_straight(dice_list):
    """Returns the sum of a small straight if found
    
    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        The integer sum of the small straight"""
    if sorted(dice_list) == [1, 2, 3, 4, 5]:
        return sum(dice_list)
    else:
        return 0


def large_straight(dice_list):
    """Returns the sum of a large straight if found
    
    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        The integer sum of the large straight"""
    if sorted(dice_list) == [2, 3, 4, 5, 6]:
        return sum(dice_list)
    else:
        return 0


def yahtzee(dice_list):
    """Returns the 50 if all dice are equal
    
    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        50 if yahtzee otherwise 0"""
    counts = dice_counts(dice_list)
    if 5 in counts.values():
        return 50
    else:
        return 0


def chance(dice_list):
    """Returns the sum of the dice_list
    
    Args:
        dice_list: a list of 5 integers indicating the dice rolled
    
    Returns:
        Integer sum of the dice_list"""
    return sum(dice_list)



class Dice:
    """Class that have information about the dice"""

    def __init__(self, num_dice=5):
        #self._fixed_dice = [False for _ in range(num_dice)]
        self._dice = [random.randint(1,6) for _ in range(num_dice)]   # randint(a, b)  -->  a <= rnd_val <= b
        self._rotation = [random.randint(0,1) for _ in self._dice]

    def roll(self, dice_to_save_idx):
        """Rolls the dice that are not in the dice_to_save idx list.
        
        Args:
            dice_to_save_idx: Integer list saying which dice not to roll
        """
        for idx, die in enumerate(self._dice):
            if idx not in dice_to_save_idx:       # Check if this die should be rolled
                self._dice[idx]     = random.randint(1,6)     # Roll this die
                self._rotation[idx] = random.randint(0,1)      # Rand the dice rotation
    
    def roll_all(self):
        """Rolls all dice"""
        self.roll([True for _ in self._dice])

    def get_dice(self):
        """
        Returns:
            An integer list representing the dice
        """
        return self._dice

    def get_dice_with_rotation(self):
        """
        Returns:
            An list of tuples(die, rotation)
        """
        return [(self._dice[idx], self._rotation[idx]) for idx in range(0, len(self._dice))]