"""This module have information about a player"""

from function_dummies import bonus, sub_sum, total

# For file=sys.stderr in exceptions
import sys

class Player:
    """This is the player class. It keeps track of a place with name, score etc"""

    def __init__(self, name, game_categories, player_categories):
        self._name = name.replace(" ", "").capitalize()
        self._game_categories = game_categories
        self._saved_values = [0] * len(game_categories)
        self._used_values = [False] * len(game_categories)
        self._player_categories = player_categories
    
    def get_name(self):
        """
        Returns:
            The player's name
        """
        return self._name

    def _sub_sum(self):
        """Sums the ones..sixes
        
        Returns:
            Integer sum of ones..sixes.
        """
        return sum(self._saved_values[0:6])  # Sum ones..sixes

    def _bonus(self):
        """Calcutes if ones..sixes have the sum of at least 63. If so an extra bonus of 50 is given.
        
        Returns:
            Integer bonus of 50 if ones..sixes sum up to at least 63, else bonus is 0.
        """
        if self._sub_sum() >= 63:
            return 50
        else:
            return 0

    def _total(self):
        """Sums all categories + bonus and returns it

        Returns:
            Integer of the total score
        """
        return sum(self._saved_values) + self._bonus()

    def get_saved_value(self, category):
        """Returns the value of the given category

        Args:
            category: The category that you will get the value of.

        Returns:
            Integer value of the category
        """
        if category in [sub_sum]:
            return self._sub_sum()
        elif category in [bonus]:
            return self._bonus()
        elif category in [total]:
            return self._total()
        else:
            try:
                idx = self._game_categories.index(category)
                return self._saved_values[idx]
            except (ValueError, IndexError) as e:
                print("Exception Error: {0}. ({1})".format(e, self.get_saved_value.__name__), file=sys.stderr)
                return 0

    def is_category_used(self, category):
        """Checks if the category is used
        
        Args:
            category: The category that you want to check

        Returns:
            Returns True if the category is used, else False"""
        if category in [sub_sum, bonus, total]:
            return True
        else:
            try:
                idx = self._game_categories.index(category)
                return self._used_values[idx]
            except (ValueError, IndexError) as e:
                print("Exception Error: {0}. ({1})".format(e, self.is_category_used.__name__), file=sys.stderr)
                return False

    def use_category(self, category, dice):
        """Sets the category to used

        Args:
            category: This category will be set to used
            dice: an integer list of dice
        """
        try:
            idx = self._game_categories.index(category)
            self._saved_values[idx] = category(dice)
            self._used_values[idx] = True
        except (ValueError, IndexError) as e:
            print("Exception Error: {0}. ({1})".format(e, self.use_category.__name__), file=sys.stderr)