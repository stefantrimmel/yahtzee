import sys
from cx_Freeze import setup, Executable

setup(
      name = "Python Yahtzee",
      version = "1.0",
      description = "Python Yahtzee by Stefan Trimmel",
      executables = [Executable("yahtzee.py")]
      )
