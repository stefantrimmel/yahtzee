"""This module shows how simple it is to instansiate the Yathzee game"""

## Import the game
from game import *


def main():
    """Creates the game environment and starts it"""
    game = Game()
    game.start_up()


if __name__ == '__main__':
    main()
    print()
    input("Press any key to exit...")

