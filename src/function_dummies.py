"""
These free functions are just dummy-functions 
so that we can use the name and put them into 
the CATEGORIES lists.
"""


def banner():
    """Dummy. just for getting a function with this name"""
    pass


def player_names():
    """Dummy. just for getting a function with this name"""
    pass


def sub_sum():
    """Dummy. just for getting a function with this name"""
    pass


def bonus():
    """Dummy. just for getting a function with this name"""
    pass


def total():
    """Dummy. just for getting a function with this name"""
    pass

#################################################