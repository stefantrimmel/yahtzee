"""This module have helper functions"""

import subprocess
import os

def clearscreen():
    """Clears the screen in the REPL"""
    if os.name == 'nt': # Windows
        subprocess.call("cls", shell=True)
        return
    else: # POSIX
        subprocess.call("clear", shell=True)
        return